const mysql = require('mysql2')

const pool = mysql.createPool({
    host: "db",
    user: 'root',
    password: 'root',
    waitForConnections: true,
    connectionLimit: 20,
    database: 'movie_db',
})

module.exports = {
    pool,
}